import re
import logging
logging.basicConfig(level=logging.DEBUG)
import big_o
import solution
import solution_checker_helper


class SolutionChecker(solution_checker_helper.SolutionCheckerTemplate):
    def __init__(self):
        self.patterns = [
            ([3,7,1,6], 5),
            ([10,1], 10),
            ([2, 199, 956, 330, 638, 281, 948, 729, 330, 792], 521),
            ([471,  68, 944, 965, 719, 350, 254, 265, 127, 526, 142, 317, 627,
                728,  24, 430, 652, 853, 475, 969, 266,  14, 484, 256, 824, 233,
                311, 791, 715, 558, 705, 419,   5,  11, 511,  83,  51, 966, 859,
                152,   1, 942, 278, 186, 692, 109, 265, 975, 639, 521, 398, 775,
                141, 967, 861, 618,  43, 701, 913, 525, 354, 120, 755, 885, 100,
                759,  17, 967, 615, 552, 296, 929, 266, 828, 985, 783, 519,  66,
                472, 438, 203, 424, 358, 164, 441, 263, 522,  35, 906, 816, 553,
                852, 962, 111, 631, 998, 988, 603, 128, 583], 634)
        ]

    def run(self):
        ##
        func = solution.Solution().run

        ##
        for nums,correct_answer in self.patterns:
            answer = func(nums)
            if len(nums)>5:
                show_nums = ','.join(map(str,nums[:5])) + ',...'
            else:
                show_nums = ','.join(map(str,nums))
            logging.info(f'{show_nums}->{answer}, correct answer: {correct_answer}')
            
            if answer==correct_answer:
                logging.info('Pass')
            else:
                logging.error('Fail')
        ##
        n = 3
        expected_tc = 'Linear'
        logging.info(f'Start checking time complexity for {n} times')
        self.check_time_complexity_repeat(func, expected_tc, n)
