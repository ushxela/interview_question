from solution import Solution

func = Solution().run

input_list = [1,2,5,10,20,100,200]
output_list = [0, 1, 6, 16, 49, 400, 952]

for input,output in zip(input_list, output_list):
    user_output = func(input)
    print(input, output, user_output)
    if output != user_output:
        print(f'Error')
    else:
        print('Correct')
