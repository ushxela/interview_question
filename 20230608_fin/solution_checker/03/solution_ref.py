from typing import List
from itertools import accumulate

class Solution(object):
    def run(self, nums: List[int]) -> int:
        total_sum = 0
        result = 0
        for index in range(len(nums)):
            total_sum += nums[index]
            result = max(result, (total_sum + index) // (index + 1))
        return int(result)

class Solution2(object):
    def run(self, A: List[int]) -> int:
        return max((a + i) // (i + 1) for i,a in enumerate(accumulate(A)))
