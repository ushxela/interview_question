import solution_checker


def main():
    solution_checker.SolutionChecker().run()


if __name__ == '__main__':
    main()
