import re
import logging
logging.basicConfig(level=logging.DEBUG)
import big_o


class SolutionCheckerTemplate():
    def check_time_complexity(self, func, expected_tc):
        logging.info(f'Start checking time complexity for function {func.__name__}')
        positive_int_generator = lambda n: big_o.datagen.integers(n, 0, 10000)
        best, others = big_o.big_o(func, positive_int_generator, n_repeats=20)
        logging.info(best)
        logging.info(f'Expected time complexity: {expected_tc}')
        if re.findall(expected_tc, str(best)):
            logging.info('Pass')
        else:
            logging.error('Fail')
        logging.info(f'End checking time complexity for function {func.__name__}')
    
    def check_time_complexity_repeat(self, func, expected_tc, i=3):
        for i in range(3):
            logging.info(f'Round {i+1}:')
            self.check_time_complexity(func, expected_tc)