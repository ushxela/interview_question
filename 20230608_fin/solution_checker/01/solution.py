from typing import List
import bisect

class Solution(object):
    def sampleStats(self, count: List[int]) -> List[float]:
        ans = [[i, count[i]] for i in range(256) if count[i]]
        prefixCount = [ans[0][::-1]]
        for num in ans[1:]:
            prefixCount.append([prefixCount[-1][0] + num[1], num[0]])
        if prefixCount[-1][0] % 2:
            median = prefixCount[bisect.bisect_left(prefixCount, [prefixCount[-1][0] // 2 + 1, 0])][1]
        else:
            median = prefixCount[bisect.bisect_left(prefixCount, [prefixCount[-1][0] // 2, 0])][1]
            median += prefixCount[bisect.bisect_left(prefixCount, [prefixCount[-1][0] // 2 + 1, 0])][1]
            median /= 2
        mean = sum([item[0] * item[1] for item in ans]) / prefixCount[-1][0]
        return [float(min(ans)[0]), float(max(ans)[0]), mean, median, float(max(ans, key = lambda x : x[1])[0])]
