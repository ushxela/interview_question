import logging
logging.basicConfig(level=logging.DEBUG)
import big_o
import solution
import solution_checker_helper


class SolutionChecker(solution_checker_helper.SolutionCheckerTemplate):
    def __init__(self):
        self.patterns = [
            (1,1),
            (2,0.5),
            (10,0.5),
            (1000,0.5),
            (10000,0.5),
        ]

    def run(self):
        ##
        func = solution.Solution().nthPersonGetsNthSeat

        ##
        for input,correct_answer in self.patterns:
            answer = func(input)
            logging.info(f'{input}->{answer}, correct answer: {correct_answer}')
            
            if answer==correct_answer:
                logging.info('Pass')
            else:
                logging.error('Fail')