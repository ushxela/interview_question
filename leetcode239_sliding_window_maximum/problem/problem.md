# Problem

## Description

Given an array of integers `nums` and a sliding window of size `k` that moves from the leftmost position to the rightmost position, the task is to find the maximum value within the window at each position. The sliding window moves one position to the right each time.

The goal is to return an array containing the maximum values of each sliding window.

### Example
When using the input array nums = [1, 2, 3, 4, 5, 6, 7, 8] and k = 4, the expected output would be [4, 5, 6, 7, 8].

Explanation:
The sliding window's initial state is [1, 2, 3, 4], and the maximum value is 4.
Then, the window shifts one step to the right, becoming [2, 3, 4, 5], and the maximum value is 5.
Further right shift leads to the window becoming [3, 4, 5, 6], with the maximum value being 6.
Another right shift results in the window becoming [4, 5, 6, 7], and the maximum value is 7.
Finally, after one more right shift, the window becomes [5, 6, 7, 8], and the maximum value is 8.