# Note for Interviewer

## Folder Structure
```
.
|____ problem
| |____ problem.md
| |____ solution.py
|____ README.md
|____ solution_checker
| |____ solution_checker_helper.py
| |____ solution.py
| |____ solution_ref.py
| |____ trial.ipynb
| |____ main.py
| |____ solution_checker.py
```
## Step
1. Copy the folder `problem` to interviewee's local location.
2. Open `problem.md` using VS Code and then open the preview.
3. Ask the interviewee to write the answer in the method of the  ```Solution``` class under the file ```solution```.
4. Copy the ```solution.py``` file to the folder ```solution_checker```.
5. Execute ```python main.py```
6. Verify that all answers are correct.
7. Confirm that the complexity is expected.