import pandas as pd


class Solution():
    def gen_return_df(self) -> pd.DataFrame:
        df = pd.read_csv('return_is.csv')
        df['Date'] = pd.to_datetime(df['Date'])
        df = df.set_index('Date')
        return df

    def gen_volume_df(self) -> pd.DataFrame:
        df = pd.read_csv('volume_is.csv')
        df['Date'] = pd.to_datetime(df['Date'])
        df = df.set_index('Date')
        return df

    def compute_weight(self, df_ret: pd.DataFrame, df_volume:pd.DataFrame) -> pd.DataFrame:
        ## TODO
        df = pd.DataFrame().reindex_like(df_ret).fillna(1)
        df = df.apply(lambda x: x/x.abs().sum(),axis=1)
        return df