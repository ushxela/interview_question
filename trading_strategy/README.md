# Note for Inverviewer
## Folder Structure
```
.
|____ problem
| |____ volume_is.csv
| |____ solution.py
| |____ __init__.py
| |____ return_is.csv
| | |____ solution.cpython-38.pyc
| | |____ solution_checker.cpython-38.pyc
| |____ problem.ipynb
| |____ solution_checker.py
|____ README.md
|____ solution_checker
| |____ solution.py
| |____ return_oos.csv
| |____ __init__.py
| |____ volume_oos.csv
| |____ __pycache__
| | |____ solution.cpython-38.pyc
| | |____ solution_checker.cpython-38.pyc
| |____ solution.ipynb
| |____ solution_checker.py
```
## Step
1. Copy the folder ```problem``` to interviewee's local location.
2. Ask the interviewee to open the file ```problem.ipynb```.
3. Ask the interviewee to read the problem and write the strategy in the ```compute_weight``` method of the ```Solution``` class under the file ```solution.py```.
4. After the interviewee finishes writing the strategy, please copy ```solution.py``` to the ```solution_checker``` folder.
5. Open the file ```solution.ipynb```, click on 'Run All' to verify 1) if the strategy written by the interviewee is a PIT strategy, 2) what is the interviewee's strategy's Sharpe ratio, and 3) whether the strategy is making money for the years 2020, 2021, and 2022, and check if there is any overfitting.
6. Ask the interviewee what type of strategy they have written and why they have constructed the strategy in that particular way.