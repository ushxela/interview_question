import pandas as pd
import numpy as np
import solution
from matplotlib import pyplot as plt

class SolutionChecker():

    def gen_return_df(self) -> pd.DataFrame:
        df = pd.read_csv('return_oos.csv')
        df['Date'] = pd.to_datetime(df['Date'])
        df = df.set_index('Date')
        return df

    def gen_volume_df(self) -> pd.DataFrame:
        df = pd.read_csv('volume_oos.csv')
        df['Date'] = pd.to_datetime(df['Date'])
        df = df.set_index('Date')
        return df

    def compute_benchmark_weight(self, df_ret: pd.DataFrame, df_volume:pd.DataFrame) -> pd.DataFrame:
        ## TODO
        df = pd.DataFrame().reindex_like(df_ret).fillna(1)
        df = df.apply(lambda x: x/x.abs().sum(),axis=1)
        return df

    def check_pit(self, func, df_ret, df_volume):
        df_w1 = func(df_ret, df_volume)
        df_w2 = func(df_ret.iloc[:-63], df_volume.iloc[:-63]).iloc[63:]
        
        ##
        df = df_w1
        df = df.loc[df_w2.index]
        df_w1 = df

        ##
        ret = df_w1.equals(df_w2)

        if ret:
            print('PIT Check: Pass')
        else:
            print('PIT Check: Fail')
        return ret

    def __compute_return(self, total_return):
        sr = total_return 
        ret = np.nanmean(sr) * 252
        ret = ret * 100
        return '{:.2f}%'.format(ret)

    def __compute_sharpe(self, total_return):
        sr = total_return 
        ret = np.nanmean(sr) / np.nanstd(sr) * np.sqrt(252)
        return '{:.2f}'.format(ret)
    
    def __compute_max_dd(self, total_return):
        sr = total_return
        sr = np.nancumsum(sr)
        sr = np.maximum.accumulate(sr) - sr
        ret = np.nanmax(sr)
        ret = ret * 100
        return '{:.2f}%'.format(ret)
    
    def __plot_perf(self, total_return):
        sr = total_return
        sr.cumsum().plot()
        plt.show()
    
    def __plot_by_year(self, total_return):
        sr = total_return
        sr = sr.groupby(sr.index.to_period('Y')).sum()
        sr.plot.bar()
        plt.show()

    def check_performance(self, func, df_ret, df_volume):

        ##
        df_w = func(df_ret, df_volume)
        df = df_w.apply(lambda x: x/x.abs().sum() if x.abs().sum()>=1 else x, axis=1)
        df_w_adj = df
        df = df_w_adj.shift(2)*df_ret
        sr = df.sum(axis=1)
        sr[sr==0] = np.nan
        sr = sr.loc[sr.first_valid_index():]
        sr = sr.fillna(0)
        total_return = sr

        ##
        res_dict = dict()
        res_dict['Return'] = self.__compute_return(total_return)
        res_dict['Sharpe'] = self.__compute_sharpe(total_return)
        res_dict['Max DD'] = self.__compute_max_dd(total_return)

        print(res_dict)

        self.__plot_perf(total_return)
        self.__plot_by_year(total_return)



        

