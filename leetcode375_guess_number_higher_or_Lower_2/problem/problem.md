# Question

## Description

Suppose we are playing the Guessing Game. Here's how it works:

I will select a number between 1 and n.
Your task is to guess the chosen number correctly to win the game.
If your guess is incorrect, I will provide a hint, indicating whether the chosen number is higher or lower than your guess.
Each time you make an incorrect guess, you will incur a cost equal to the value of your guess.
If you run out of money before guessing the correct number, you will lose the game.
Given a specific value of n, you need to determine the minimum amount of money required to guarantee a win, regardless of the number I choose.
